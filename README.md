# Background Layer Blender Addon

Simple Blender addon which sets the selected active object to a locked state and displays it as wireframe. Simulates behaviour of LightWave Modeler's background layers.

## Usage

Install, and open the properties in the view. Look for "Background Layer".  

Select an object, and click the "background Layer" button to activate it. To turn this off for an object, you must select the object in the outliner, and click the "Visible Layer" button.

Only works with the active object. Assign to a shortcut for a faster workflow.
