# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# This addon was generated with the Visual Scripting Addon.
# You can find the addon under https://blendermarket.com/products/serpens
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
sn_tree_name = "Background Layer"
addon_keymaps = []

bl_info = {
    "name": "Background Layer",
    "author": "Herbert van der Wegen",
    "description": "Selected active object is locked and display set to wire.",
    "location": "",
    "doc_url": "",
    "warning": "",
    "category": "Background Layer",
    "blender": (2,91,0),
    "version": (0,3,3)
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# IMPORTS
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import bpy
from bpy.app.handlers import persistent


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# UTILITY FUNCTIONS
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def sn_print(*text):
    text = ', '.join(map(str, text))
    print(text) # actual print command
    try: # try to find the area in which the addon is opened and add the print text
        for area in bpy.context.screen.areas:
            if area.type == "NODE_EDITOR":
                if area.spaces[0].node_tree:
                    if area.spaces[0].node_tree.bl_idname == "ScriptingNodesTree":
                        if sn_tree_name == area.spaces[0].node_tree.name:
                            bpy.context.scene.sn_properties.print_texts.add().text = str(text)

        for area in bpy.context.screen.areas:
            area.tag_redraw()
    except: pass
    
def get_enum_identifier(enumItems, name):
    for item in enumItems:
        if item.name == name:
            return item.identifier
            
    return ''

def report_sn_error(self,error):
    self.report({"ERROR"},message="There was an error when running this operation! It has been printed to the console.")
    print("START ERROR | Node Name: ",self.name," | (If you are this addons developer you might want to report this to the Serpens team) ")
    print("")
    print(error)
    print("")
    print("END ERROR - - - - ")
    print("")
    
def get_python_filepath():
    path = os.path.dirname(bpy.data.filepath)
    try:
        __file__
        exported = True
    except:
        exported = False
    if exported:
        path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    return path


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CODE
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
class SNA_OT_Operator_abc640b71e(bpy.types.Operator):
    bl_idname = "scripting_nodes.sna_ot_operator_abc640b71e"
    bl_label = "ActivateBackgroundLayer"
    bl_description = "My Operators description"
    bl_options = {"REGISTER","UNDO"}
    
    @classmethod
    def poll(cls, context):
        return True
        
    def execute(self, context):
        try:
            pass
            bpy.context.active_object.display_type = get_enum_identifier(bpy.context.active_object.bl_rna.properties['display_type'].enum_items, r"Wire")
            bpy.context.active_object.hide_select = True
            
        except Exception as exc:
            report_sn_error(self,exc)
        return {"FINISHED"}
        
    def draw(self, context):
        layout = self.layout
    
    
class SNA_OT_Operator_567ea3707d(bpy.types.Operator):
    bl_idname = "scripting_nodes.sna_ot_operator_567ea3707d"
    bl_label = "DeActivateBackgroundLayer"
    bl_description = "My Operators description"
    bl_options = {"REGISTER","UNDO"}
    
    @classmethod
    def poll(cls, context):
        return True
        
    def execute(self, context):
        try:
            pass
            bpy.context.active_object.display_type = get_enum_identifier(bpy.context.active_object.bl_rna.properties['display_type'].enum_items, r"Textured")
            bpy.context.active_object.hide_select = False
            
        except Exception as exc:
            report_sn_error(self,exc)
        return {"FINISHED"}
        
    def draw(self, context):
        layout = self.layout
    
    
class SNA_OT_BTN_296d5b6818(bpy.types.Operator):
    bl_idname = 'scripting_nodes.sna_ot_btn_296d5b6818'
    bl_label = r"Background Layer"
    bl_description = r""
    bl_options = {"REGISTER","INTERNAL"}
    
    def execute(self, context):
        try:
            pass
            bpy.ops.scripting_nodes.sna_ot_operator_abc640b71e('INVOKE_DEFAULT')
            
        except Exception as exc:
            report_sn_error(self,exc)
        return {"FINISHED"}
        
class SNA_OT_BTN_f2d6c1a817(bpy.types.Operator):
    bl_idname = 'scripting_nodes.sna_ot_btn_f2d6c1a817'
    bl_label = r"Visible Layer"
    bl_description = r""
    bl_options = {"REGISTER","INTERNAL"}
    
    def execute(self, context):
        try:
            pass
            bpy.ops.scripting_nodes.sna_ot_operator_567ea3707d('INVOKE_DEFAULT')
            
        except Exception as exc:
            report_sn_error(self,exc)
        return {"FINISHED"}
        
class SNA_PT_aebb36ae11(bpy.types.Panel):
    bl_label = "Background Layer"
    bl_idname = "SNA_PT_aebb36ae11"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Background Layer"
    
    @classmethod
    def poll(cls, context):
        return True
    
    def draw_header(self, context):
        layout = self.layout
    
    def draw(self, context):
        layout = self.layout
        layout.operator("scripting_nodes.sna_ot_btn_296d5b6818",text=r"Background Layer",emboss=True,depress=False,icon="MOD_WIREFRAME")
        layout.operator("scripting_nodes.sna_ot_btn_f2d6c1a817",text=r"Visible Layer",emboss=True,depress=False,icon="SNAP_VOLUME")
    
    

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# PROPERTIES
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# REGISTER / UNREGISTER
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def register():
    bpy.utils.register_class(SNA_OT_Operator_abc640b71e)
    bpy.utils.register_class(SNA_OT_Operator_567ea3707d)
    bpy.utils.register_class(SNA_OT_BTN_296d5b6818)
    bpy.utils.register_class(SNA_OT_BTN_f2d6c1a817)
    bpy.utils.register_class(SNA_PT_aebb36ae11)

def unregister():
    global addon_keymaps
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    bpy.utils.unregister_class(SNA_OT_Operator_abc640b71e)
    bpy.utils.unregister_class(SNA_OT_Operator_567ea3707d)
    bpy.utils.unregister_class(SNA_OT_BTN_296d5b6818)
    bpy.utils.unregister_class(SNA_OT_BTN_f2d6c1a817)
    bpy.utils.unregister_class(SNA_PT_aebb36ae11)